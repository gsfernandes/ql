import setuptools

setuptools.setup(
    name="ql",
    version=0,
    author="Gavin S. Fernandes",
    author_email="gavinfernandes2012@gmail.com",
    description="A package for sample-continuous query learning",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'tqdm',
    ]
)
