from ql import DataTape, QueryGen
import tensorflow as tf
from tensorflow import keras as K
import sys
from tqdm import tqdm, trange


class ModelHandler:
    def __init__(self, model, function, session, tape=None, verbose=4):
        if tape is None:
            self.tape = DataTape()
            self._private_tape = True
        elif isinstance(tape, str):
            self.tape = DataTape(tape)
            self._private_tape = True
        else:
            self.tape = tape
            self._private_tape = False
        self.model = model
        self.function = function
        self.sess = session
        self.qgen = None
        self.verbose = verbose

    def __del__(self):
        if self._private_tape:
            self.tape.close()

    def compile(
        self,
        qgen_initializer=K.initializers.random_uniform(-1, 1),
        qgen_optimizer=tf.train.AdamOptimizer(10),
    ):
        self.qgen = QueryGen(
            self.model, self.sess, initializer=qgen_initializer
        ).compile(qgen_optimizer)
        return self

    def fit(self, iterations_list):
        for idx in range(0, len(iterations_list), 2):
            self.generate(iterations_list[idx])
            try:
                self.train(iterations_list[idx + 1])
            except IndexError:
                self.train(iterations_list[idx - 1])

    def generate(self, num):
        iterable = trange(num, desc="GEN") if self.verbose == 4 else range(num)
        for _ in iterable:
            query = self.qgen(10)
            self.tape.record(self.function(query), query)
        self.tape.on_generation_end()

    def train(self, epochs=3):
        if self.verbose in range(0, 4):
            callbacks = None
            keras_verbose = self.verbose
        else:
            pbar = tqdm(total=(epochs * len(self.tape)), desc="TRN")
            callbacks = [
                K.callbacks.LambdaCallback(
                    on_batch_end=lambda epoch, logs: pbar.update(1)
                )
            ]
            keras_verbose = 0

        fit_return = self.model.fit_generator(
            self.tape, epochs=epochs, verbose=keras_verbose, callbacks=callbacks
        )

        pbar.close() if self.verbose == 4 else None

        return fit_return
