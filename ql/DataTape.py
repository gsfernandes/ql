import tensorflow as tf
import pandas as pd
import numpy as np


class DataTape(tf.keras.utils.Sequence):
    def __init__(self, file_path="default.h5", shuffle=True):
        self.file_path = file_path
        self.ds_store = pd.HDFStore(self.file_path)
        self.new_data = {"labels": [], "features": []}
        self.shuffle = shuffle
        self.shuffle_map = None
        self.on_epoch_end()

    def record(self, label, features):
        self.new_data["labels"].append(label)
        self.new_data["features"].append(features)

    def close(self):
        self.ds_store.close()

    def __len__(self):
        try:
            labels_len = len(self.ds_store.get("labels"))
            features_len = len(self.ds_store.get("features"))
            assert labels_len == features_len
        except KeyError:
            return 0
        return labels_len

    def __getitem__(self, index):
        feature = self.ds_store.get("features").iloc[self.shuffle_map[index]].values
        label = self.ds_store.get("labels").iloc[self.shuffle_map[index]].values
        # Keras needs an array of features for a feature batch
        feature_batch = np.array([feature])
        # The same goes for labels
        label_batch = np.array([label])
        return feature_batch, label_batch

    def on_epoch_end(self):
        self._update_shuffle_map()

    def on_generation_end(self):
        tape_len = len(self)
        new_data_len = self._new_data_len()
        index_list = list(range(tape_len, tape_len + new_data_len))
        self.ds_store.append(
            "labels", pd.DataFrame(self.new_data["labels"], index=index_list)
        )
        self.ds_store.append(
            "features", pd.DataFrame(self.new_data["features"], index=index_list)
        )
        self.ds_store.flush(fsync=True)
        self._update_shuffle_map()

    def _update_shuffle_map(self):
        self.shuffle_map = np.arange(0, len(self))
        if self.shuffle:
            np.random.shuffle(self.shuffle_map)

    def _new_data_len(self):
        labels_len = len(self.new_data["labels"])
        features_len = len(self.new_data["features"])
        assert labels_len == features_len
        return labels_len
