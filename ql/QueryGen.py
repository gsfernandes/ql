import tensorflow as tf
from tensorflow import keras as K
from warnings import warn
from operator import itemgetter


class QueryGen:
    def __init__(
        self,
        model,
        session,
        sample_num=32,
        initializer=K.initializers.random_uniform(-1, 1),
        domain=(-1, 1),
        noise_stddev=0.01,
    ):
        self.model = model
        self.sess = session

        is_initializer_instance = isinstance(initializer, K.initializers.Initializer)
        try:
            assert is_initializer_instance
        except AssertionError:
            raise TypeError(
                initializer + " is not an instance of a K.initializers.Initializer"
            )

        self.query = self._input_variable(model, initializer, domain)
        self.uncertainty = self._variance_tensor(
            model, self.query, noise_stddev, sample_num
        )
        self.optimize = None

    def compile(self, optimizer=tf.train.AdamOptimizer(learning_rate=10)):
        self.optimize = optimizer.minimize(tf.math.negative(self.uncertainty))
        self.sess.run(tf.global_variables_initializer())
        return self

    def __call__(self, iterations=100):
        queries = []
        uncertainties = []
        for query, uncertainty in self.erator(iterations=iterations, generations=1):
            queries.append(query)
            uncertainties.append(uncertainty)
        max_uncertainty = max(uncertainties)
        max_uncertainty_idx = uncertainties.index(max_uncertainty)
        max_uncertainty_query = queries[max_uncertainty_idx]
        return max_uncertainty_query

    def erator(self, iterations=100, generations=1):
        try:
            assert (iterations % generations) == 0
        except AssertionError:
            warn(
                "iterations is not divisible by generations, some iterations won't be executed"
            )

        try:
            assert self.optimize is not None
        except AssertionError:
            warning = (
                "The optimizer has not been initialized,"
                + " calling the QueryGen.compile() method with sane defaults"
            )
            warn(warning)
            self.compile()

        self.sess.run(tf.variables_initializer((self.query,)))

        for gen in range(generations):
            self.model.trainable = False
            for iter_ in range(int(iterations / generations)):
                self.sess.run(self.optimize)
            self.model.trainable = True
            yield self.sess.run(self.query)[0], self.sess.run(self.uncertainty)

    def _variance_tensor(self, model, query, noise_stddev, sample_num):
        model_outs = self._multiple_noisy_layers(model, query, noise_stddev, sample_num)
        variance = tf.nn.moments(model_outs, 0)[1]
        return variance

    @staticmethod
    def _input_variable(model, query_initializer, variable_range):
        """Creates an input variable that can be passed to 'model'"""
        input_shape = [1] + list(model.layers[0].input_shape[1:])
        return tf.Variable(
            initial_value=tf.clip_by_value(
                query_initializer(shape=input_shape),
                variable_range[0],
                variable_range[1],
            ),
            trainable=True,
            name="input_variable",
            expected_shape=input_shape,
            constraint=lambda x: tf.clip_by_value(
                x, variable_range[0], variable_range[1]
            ),
        )

    @staticmethod
    def _multiple_noisy_layers(model, inputs, noise_stddev, sample_num):
        """Places several gaussian noise layers, one each behind several models,
        and returns several outputs."""
        model_outs = []
        for _ in range(sample_num):
            gauss = K.layers.GaussianNoise(noise_stddev)(inputs, training=True)
            model_outs.append(model(gauss))

        model_outs = tf.concat(model_outs, 0)

        return model_outs
