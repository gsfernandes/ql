from abc import ABC, abstractmethod
from numpy import array


class AutoTune(ABC):
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def __call__(self, model_handler):
        pass


class PropAutoTune(AutoTune):
    def __init__(self, aux_train_gain, aux_gen_gain):
        self.epochs = PropController(aux_train_gain, co_max=20)
        self.generations = PropController(5, aux_train_gain, co_min=5, co_max=30)
        self.first_call = True
        self.prev_loss = 0
        self.curr_loss = 0
        self.new_data_loss = 0

    def __call__(self, model_handler):
        while True:
            generations = self.generations(self.new_data_loss - self.curr_loss)
            model_handler.generate(generations)

            self.epochs.multiplier = len(model_handler.tape)
            epochs = self.epochs(self.prev_loss - self.new_data_loss)
            model_handler.train(epochs)

            self.prev_loss = self.curr_loss
            self.curr_loss = self._get_loss(
                model_handler.model.evaluate_generator(model_handler.tape)
            )

            new_data = model_handler.tape.new_data
            train_x, train_y = array(new_data["features"]), array(new_data["labels"])
            self.new_data_loss = self._get_loss(
                model_handler.model.evaluate(train_x, train_y, verbose=0)
            )
            print(self.new_data_loss)

    @staticmethod
    def _get_loss(loss_or_list):
        if isinstance(loss_or_list, list):
            return loss_or_list[0]
        else:
            return loss_or_list


class PropController:
    def __init__(
        self, cost=1, auxiliary_gain=1, auxiliary_output_sig=1, co_min=1, co_max=30
    ):
        self.aux_gain = auxiliary_gain
        self.curr_out_sig = auxiliary_output_sig
        self.co_min = co_min
        self.co_max = co_max
        # Error Delta History:
        self.ed_history = []
        # Controller output Delta History:
        self.cd_history = []
        self.prev_error = None
        self.decay = 0.1
        self.__cost = 1
        self.cost = 1

    def __call__(self, error):
        if self.prev_error is not None:
            self.ed_history.append(error - self.prev_error)
        self.prev_error = error

        cd = int(error * self.gain())
        # Restrict output to a minimum of 1
        cd = self.limit(cd + self.curr_out_sig) - self.curr_out_sig
        self.cd_history.append(cd)

        self.curr_out_sig += cd

        return round(self.curr_out_sig)

    def gain(self):
        if self.aux_gain is None:
            assert len(self.ed_history) == len(self.cd_history)
            ed_cum = 0
            cd_cum = 0
            weight = 1
            for idx in range(len(self.ed_history), 0):
                ed_cum += self.ed_history[idx] * weight
                cd_cum += self.cd_history[idx] * weight
                weight *= 1 - self.decay
            gain = cd_cum / ed_cum
        else:
            gain = self.aux_gain
        return gain

    def limit(self, co):
        if co > self.co_max:
            co = self.co_max
        if co < self.co_min:
            co = self.co_min
        return round(co)

    @property
    def cost(self):
        return self.__cost

    @cost.setter
    def cost(self, cost):
        cost_ratio = self.__cost / cost
        self.cd_history = [cost_ratio * cd for cd in self.cd_history]
        self.__cost = cost
