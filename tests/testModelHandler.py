import unittest
from unittest.mock import Mock, call
import os
import ql
from tensorflow import keras as K

# Silence benign numpy warnings
from tests.silence import silence


class TestModelHandler(unittest.TestCase):
    def setUp(self):
        silence()
        self.simple_model = K.models.Sequential([K.layers.Dense(1, input_shape=(1,))])
        self.simple_model.compile(
            optimizer=K.optimizers.Adam(500),
            loss="mean_squared_error",
            metrics=["mean_absolute_error"],
        )
        self.modhand_simple = ql.ModelHandler(
            self.simple_model, self.simple_f, K.backend.get_session(), verbose=0
        )
        self.modhand_simple.compile()

    @staticmethod
    def simple_f(x):
        return x + 2

    def test_generate(self):
        init_tape_len = len(self.modhand_simple.tape)
        self.modhand_simple.generate(3)
        final_tape_len = len(self.modhand_simple.tape)
        self.assertGreater(final_tape_len, init_tape_len)

    def test_train(self):
        self.modhand_simple.train(epochs=3)

    def test_fit(self):
        self.modhand_simple.generate = Mock()
        self.modhand_simple.train = Mock()

        self.modhand_simple.fit(range(1, 6))

        gen_call_args_list = self.modhand_simple.generate.call_args_list
        expected_gen_call_args_list = [call(1), call(3), call(5)]
        train_call_args_list = self.modhand_simple.train.call_args_list
        expected_train_call_args_list = [call(2), call(4), call(4)]

        self.assertEqual(gen_call_args_list, expected_gen_call_args_list)
        self.assertEqual(train_call_args_list, expected_train_call_args_list)
