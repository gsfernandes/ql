import unittest
import os
import ql.DataTape
import numpy as np
from tests.silence import silence


class TestDataTape(unittest.TestCase):
    @staticmethod
    def remove_testh5():
        try:
            os.remove("test.h5")
        except FileNotFoundError:
            pass

    def setUp(self):
        silence()
        self.remove_testh5()
        self.addCleanup(self.remove_testh5)

    def test_init(self):
        ql.DataTape("test.h5").close()

    def test_record(self):
        tape = ql.DataTape("test.h5")
        tape.record(1, [2, 3, 4, 5])
        tape.on_generation_end()
        self.assertTrue(np.array_equal(tape[0][0], np.array([[2, 3, 4, 5]])))
        self.assertTrue(np.array_equal(tape[0][1], np.array([[1]])))
        tape.close()
