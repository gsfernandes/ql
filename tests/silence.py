import os
import warnings


def silence():
    # Silence tensorflow:
    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
    # Silence numpy:
    warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
    warnings.filterwarnings("ignore", message="numpy.dtype size changed")
