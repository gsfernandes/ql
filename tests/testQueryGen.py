import unittest
from tensorflow import keras as K
import ql.QueryGen
from tests.silence import silence


class TestQueryGen(unittest.TestCase):
    def setUp(self):
        silence()
        self.model = K.Sequential(
            [K.layers.Dense(32, activation="relu", input_dim=4), K.layers.Dense(1)]
        )
        self.model.compile(
            optimizer="rmsprop", loss="binary_crossentropy", metrics=["accuracy"]
        )

    def test_init(self):
        ql.QueryGen(self.model, K.backend.get_session())

    def test_invalid_initializers(self):
        test_cases = ["string", 1, 1.0, [], {}, [1.0], {"key": "value"}]
        for test_case in test_cases:
            with self.subTest(initializer=test_case):
                with self.assertRaises(TypeError):
                    ql.QueryGen(
                        self.model, K.backend.get_session(), initializer=test_case
                    )

    def test_compile(self):
        ql.QueryGen(self.model, K.backend.get_session()).compile()

    def test_generator(self):
        qgen = ql.QueryGen(self.model, K.backend.get_session()).compile()
        queries = []
        uncertainties = []

        for query, uncertainty in qgen.erator(generations=2, iterations=2000):
            queries.append(query)
            uncertainties.append(uncertainty)
        self.assertLess(uncertainties[0], uncertainties[-1])
        self.assertEqual(query.shape, (4,))

    def test_call(self):
        ql.QueryGen(self.model, K.backend.get_session()).compile()()
